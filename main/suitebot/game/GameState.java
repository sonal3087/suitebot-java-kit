package suitebot.game;

import java.util.List;
import java.util.Set;

public interface GameState
{
	/**
	 * Returns the width of the game plan.
	 *
	 * @return the width of the game plan
	 */
	int getPlanWidth();

	/**
	 * Returns the height of the game plan.
	 *
	 * @return the height  of the game plan
	 */
	int getPlanHeight();

	/**
	 * Returns the list of the IDs of all bots, including the dead ones.
	 *
	 * @return the list of the IDs of all bots
	 */
	List<Integer> getAllBotIds();

	/**
	 * Returns the set of the IDS of all live bots, i.e. the bots that are still active in the game.
	 *
	 * @return the set of the IDS of all live bots
	 */
	Set<Integer> getLiveBotIds();

	/**
	 * Returns the coordinates of the location of the bot on the game plan.
	 *
	 * @param botId ID of the bot
	 * @return the location of the bot or null if the bot is dead
	 * @throws IllegalArgumentException if the bot ID is unknown
	 */
	Point getBotLocation(int botId);

	/**
	 * Returns current energy of the bot. The energy is a non-negative number.
	 *
	 * @param botId ID of the bot
	 * @return current energy of the bot
	 * @throws IllegalArgumentException if the bot ID is unknown
	 */
	int getBotEnergy(int botId);

	/**
	 * Returns the set of coordinates of all obstacles on the game plan.
	 *
	 * @return the set of coordinates of all obstacles
	 */
	Set<Point> getObstacleLocations();

	/**
	 * Returns the set of coordinates of all treasures on the game plan.
	 *
	 * @return the set of coordinates of all treasures
	 */
	Set<Point> getTreasureLocations();

	/**
	 * Returns the set of coordinates of all batteries on the game plan.
	 *
	 * @return the set of coordinates of all batteries
	 */
	Set<Point> getBatteryLocations();
}
